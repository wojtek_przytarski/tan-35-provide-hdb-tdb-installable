Instalacja HDB i TDB
========================
1. Pobierz Mambo
----------------------------
1. Pobierz https://sourceforge.net/projects/tango-cs/files/tools/ArchivingRoot-16.2.4.zip/download
2. Wypakuj do folderu `C:\tango-root\mambo`

2. Tworzenie HDB i TDB
------------------------
1. Stwórz zmienną systemową ARCHIVING_ROOT z wartością `C:\tango-root\mambo`
2. Jeśli zainstalowałeś MySQL 5.5 dla Tango to w plikach `my-hdb-createMYISAM.sql` oraz `my-tdb-createMYISAM` zamień `ENGINE=MYISAM` na `TYPE=MYISAM`. W innym wypadku pomiń ten krok.
3. Wklej zawartość plików do konsoli MySQL.

3. Stworzenie archiving devices
--------------------------------
1. W konsoli MySQL wklej zawartość pliku `my-hdb-device-install.sql`
2. W konsoli MySQL wklej zawartość `my-tdb-device-install.sql`

4. Konfiguracja archiving devices
-----------------------------------
1. Otwórz Jive
2. Dodaj brakujące klasy w poniższy sposób ![](https://preview.ibb.co/c7eT25/1.jpg) ![](https://image.ibb.co/kg8z9k/2.jpg)
3. Usuń wszystkie urządzenia `t/t/t`. Widok powinien być taki jak poniżej ![](https://image.ibb.co/jgxsUk/3.jpg)
4. Zmień właściwości aby były takie jak te poniżej ![](https://preview.ibb.co/kbjvh5/4.jpg) ![](https://preview.ibb.co/bOkCUk/5.jpg) ![](https://preview.ibb.co/jNvCUk/6.jpg) ![](https://image.ibb.co/iMyo25/7.jpg)
5. Dodaj do properties urządzeń nową własność `DbType` z wartością MySQL

5. Uruchom archiving devices
----------------------------------
1. Uruchom cmd
2. Wywołaj `C:\tango-root\mambo\devices\win32\HdbArchiver.bat 01`. Jeśli wyświetli `ConnectionFactory.connect_auto (MySql): hdbarchiver@hdb OK` to znaczy że urządzenie jest dobrze połączone z bazą. Nie zamykaj okna.
3. W nowym oknie cmd wywołaj `C:\tango-root\mambo\devices\win32\HdbArchiver.bat 01`. Jeśli wyświetli `ConnectionFactory.connect_auto (MySql): tdbarchiver@tdb OK` to znaczy że urządzenie jest dobrze połączone z bazą. Nie zamykaj okna.

6. Uruchom Mambo
-----------------------------------
1. Uruchom cmd
2. Wywołaj `C:\tango-root\mambo\bin\win32\mambo-rw.bat` lub `C:\tango-root\mambo\bin\win32\mambo.bat`. `mambo-rw.bat` pozwala na tworzenie własnych Archiving Configuration i View Configuration, a `mambo.bat` tylko wizualizować dane.
3. Stwórz użytkownika i wybierz ścieżkę